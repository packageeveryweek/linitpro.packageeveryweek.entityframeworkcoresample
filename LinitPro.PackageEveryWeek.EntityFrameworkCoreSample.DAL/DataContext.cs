﻿using System;
using Castle.Core.Configuration;
using LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.DAL;
using LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.Entities;
using Microsoft.EntityFrameworkCore;

namespace LinitPro.PackageEveryWeek.EntityFrameworkCoreSample
{
    public partial class DataContext: DbContext
    {
        private readonly IConfiguration configuration;
        
        public DataContext(
            DbContextOptions<DataContext> options,
            IConfiguration configuration): base(options)
        {
            this.configuration = configuration ??
                                 throw new ArgumentNullException(nameof(configuration));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserEntityConfiguration).Assembly);
            
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // конфигурация провайдера БД
                optionsBuilder.UseNpgsql("", b =>
                {
                    b.SetPostgresVersion(new Version(14, 0));
                    b.MigrationsAssembly("LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.DAL");
                    b.MigrationsHistoryTable("_migrations_history", DB.Scheme);
                });
            }
            
            base.OnConfiguring(optionsBuilder);
        }
    }
}