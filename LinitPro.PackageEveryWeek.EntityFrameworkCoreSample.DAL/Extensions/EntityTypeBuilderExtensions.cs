﻿using LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.EntittiesInterfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.DAL.Extensions
{
    public static class EntityTypeBuilderExtensions
    {
        public static EntityTypeBuilder<TEntity> ToNamedTable<TEntity>(
            this EntityTypeBuilder<TEntity> builder,
            string name)
            where TEntity: class
        {
            builder.ToTable(name, DB.Scheme);
            return builder;
        }
    }
}