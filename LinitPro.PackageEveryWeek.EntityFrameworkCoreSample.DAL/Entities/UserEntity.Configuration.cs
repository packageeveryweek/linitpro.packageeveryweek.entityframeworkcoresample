﻿using LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.DAL.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.Entities
{
    public class UserEntityConfiguration: IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToNamedTable("users");
            throw new System.NotImplementedException();
        }
    }
}