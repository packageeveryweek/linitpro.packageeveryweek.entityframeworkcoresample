﻿using System;
using LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.EntittiesInterfaces;

namespace LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.Entities
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class UserEntity: ISoftlyDeletableEntity
    {
        public long Id { set; get; }
        
        public bool Deleted { get; set; }
    }
}