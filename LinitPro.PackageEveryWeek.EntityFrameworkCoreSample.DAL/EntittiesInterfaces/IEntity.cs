﻿using System;

namespace LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.EntittiesInterfaces
{
    public interface IEntity
    {
        long Id { set; get; }
    }
}