﻿namespace LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.EntittiesInterfaces
{
    public interface ISoftlyDeletableEntity: IEntity
    {
        bool Deleted { set; get; }
    }
}