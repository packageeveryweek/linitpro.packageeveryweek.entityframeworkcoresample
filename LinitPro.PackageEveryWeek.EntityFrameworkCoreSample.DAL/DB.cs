﻿namespace LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.DAL
{
    public static class DB
    {
        public const string Scheme = "test";
        
        public static class Tables
        {
            public const string Users = "users";

            public const string Groups = "groups";

            public const string Roles = "roles";
        }
    }
}