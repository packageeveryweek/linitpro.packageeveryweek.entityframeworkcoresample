﻿using LinitPro.PackageEveryWeek.EntityFrameworkCoreSample.Entities;
using Microsoft.EntityFrameworkCore;

namespace LinitPro.PackageEveryWeek.EntityFrameworkCoreSample
{
    public partial class DataContext
    {
        /// <summary>
        /// Пользователи
        /// </summary>
        public DbSet<UserEntity> Users { set; get; }
        
        /// <summary>
        /// Группы
        /// </summary>
        public DbSet<GroupEntity> Groups { set; get; }
        
        /// <summary>
        /// Роли
        /// </summary>
        public DbSet<RolesEntity> Roles { set; get; }
    }
}