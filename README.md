## Технологии
* dotnet 6
* entity framework core
* npgsql for entity framework core
* postgres 13

## Быстрый старт
* Создать БД 
```
Server=localhost;Database=canary_database;User Id=postgres;Password=123456;
```
* Выполнить в консоли
```bash
cd LinitPro.PackageEveryWeek.EntityFrameworkCoreSample
dotnet run
```

## Источники
* [https://metanit.com/sharp/entityframeworkcore/](https://metanit.com/sharp/entityframeworkcore/)
* [https://docs.microsoft.com/ru-ru/ef/](https://docs.microsoft.com/ru-ru/ef/)
